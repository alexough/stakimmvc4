﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StakimMVC4.Controllers
{
    public class MassController : Controller
    {
        //
        // GET: /Mass/

        private DateTime FindLatestWeekDate(StakimDataContext dataContext)
        {
            var query = from r in dataContext.Resources
                        where r.sermon == 1
                        orderby r.week_date descending
                        select r;

            return query.First().week_date;
        }

        private DateTime FindEarliestWeekDate(StakimDataContext dataContext)
        {
            var query = from r in dataContext.Resources
                        where r.sermon == 1
                        orderby r.week_date
                        select r;

            return query.First().week_date;
        }

        private List<Models.BibleContentData> FindBibleContent(StakimDataContext dataContext, int group_id, int chapter, int from_num, int to_num)
        {
            if (to_num == -1)
            {
                to_num = 1000000;
            }

            List<Models.BibleContentData> contents = dataContext.BibleContents
                .Where(c => c.group_id == group_id && c.chapter == chapter && c.num >= from_num && c.num <= to_num)
                .OrderBy(c => c.num)
                .Select(c => new Models.BibleContentData
                {
                    Chapter = c.chapter,
                    Number = c.num,
                    Content = c.content
                }).ToList();

            return contents;
        }

        public ActionResult Gospel(string week_date)
        {
            StakimDataContext dataContext = new StakimDataContext();

            var query = dataContext.BibleGospels.Where(g => g.week_date == System.DateTime.Parse(week_date));
            Models.BibleGospelData bible_gospel = new Models.BibleGospelData();
            bible_gospel.WeekTitle = query.First().week_title;
            bible_gospel.WeekDate = System.DateTime.Parse(query.First().week_date.ToString()).ToString("yyyy년 M월 d일");
            bible_gospel.WeekDateRaw = System.DateTime.Parse(query.First().week_date.ToString()).ToString("MM-dd-yy");
            bible_gospel.GospelTitle = query.First().BibleGroup.name;

            List<Models.BibleContentData> bible_contents = new List<Models.BibleContentData>();
            foreach (BibleGospel gospel in query)
            {
                for(int chapter = gospel.from_chapter; chapter <= gospel.to_chapter; chapter++)
                {
                    int from_num = gospel.from_num;
                    if(chapter > gospel.from_chapter)   from_num = 0;
                    int to_num = gospel.to_num;
                    if (chapter < gospel.to_chapter)    to_num = -1;
                    List<Models.BibleContentData> contents = FindBibleContent(dataContext, gospel.group_id, chapter, from_num, to_num);
                    bible_contents.AddRange(contents);
                }
            }

            bible_gospel.ContentList = bible_contents;

            return View(bible_gospel);
        }

        public ActionResult All()
        {
            StakimDataContext dataContext = new StakimDataContext();

            DateTime latest_week = FindLatestWeekDate(dataContext);
            DateTime earliest_week = FindEarliestWeekDate(dataContext);

            var gospel_list = dataContext.BibleGospels
                .Where(g => g.week_date <= latest_week && g.week_date >= earliest_week)
                .Select(g => new Models.SimpleBibleGospelData
                {
                    WeekDate = g.week_date,
                    WeekTitle = g.week_title
                })
                .Distinct()
                .OrderByDescending(g => g.WeekDate)
                .ToList();

            return View(gospel_list);
        }

        public ActionResult Index()
        {
            StakimDataContext dataContext = new StakimDataContext();

            DateTime latest_week = FindLatestWeekDate(dataContext);

            int count = dataContext.BibleGospels.Where(g => g.week_date == latest_week).Count();
            while (count <= 0)
            {
                latest_week = latest_week.AddDays(-7);
                count = dataContext.BibleGospels.Where(g => g.week_date == latest_week).Count();
            }

            var query = dataContext.BibleGospels.Where(g => g.week_date == latest_week);
            Models.BibleGospelData bible_gospel = new Models.BibleGospelData();
            bible_gospel.WeekTitle = query.First().week_title;
            bible_gospel.WeekDate = System.DateTime.Parse(query.First().week_date.ToString()).ToString("yyyy년 M월 d일");
            bible_gospel.WeekDateRaw = System.DateTime.Parse(query.First().week_date.ToString()).ToString("MM-dd-yy");
            bible_gospel.GospelTitle = query.First().BibleGroup.name;

            List<Models.BibleContentData> bible_contents = new List<Models.BibleContentData>();
            foreach (BibleGospel gospel in query)
            {
                for (int chapter = gospel.from_chapter; chapter <= gospel.to_chapter; chapter++)
                {
                    int from_num = gospel.from_num;
                    if (chapter > gospel.from_chapter) from_num = 0;
                    int to_num = gospel.to_num;
                    if (chapter < gospel.to_chapter) to_num = -1;
                    List<Models.BibleContentData> contents = FindBibleContent(dataContext, gospel.group_id, chapter, from_num, to_num);
                    bible_contents.AddRange(contents);
                }
            }

            bible_gospel.ContentList = bible_contents;

            return View(bible_gospel);
        }

    }
}
