﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StakimMVC4.Controllers
{
    public class BaseBoardController : Controller
    {
        private int FindNumBoards(StakimDataContext dataContext, string page_name)
        {
            return dataContext.Boards.Where(b => b.BoardTitle.page == page_name).Count();
        }

        private List<Models.SubBoardData> FindSubBoards(StakimDataContext dataContext, Models.BoardData board)
        {
            List<Models.SubBoardData> list = dataContext.SubBoards.Where(s => s.board_id == board.Id).OrderBy(s => s.id)
                .Select(s => new Models.SubBoardData
                {
                    Id = s.id,
                    Board = board,
                    SubItem = s.sub_item
                }).ToList();

            return list;
        }

        protected List<Models.BoardData> FindBoard(int? s, int? r, string page_name)
        {
            StakimDataContext dataContext = new StakimDataContext();

            int start = 0;
            if (s != null) start = s.Value;
            int num = 15;
            if (r != null) num = r.Value;

            ViewBag.start_index = start;
            ViewBag.num_rows = num;
            ViewBag.num_all_boards = FindNumBoards(dataContext, page_name);

            List<Models.BoardData> list = dataContext.Boards.Where(b => b.BoardTitle.page == page_name)
                .OrderByDescending(b => b.week_date).ThenBy(b => b.id)
                .Skip(start).Take(num)
                .Select(b => new Models.BoardData
                {
                    Id = b.id,
                    BoardTitle = b.BoardTitle.title,
                    Item = b.item,
                    WeekDate = System.DateTime.Parse(b.week_date.ToString()).ToString("yyyy-MM-dd"),
                }).ToList();

            foreach (Models.BoardData board in list)
            {
                List<Models.SubBoardData> sub_board_list = FindSubBoards(dataContext, board);
                board.SubBoards = sub_board_list;
            }

            return list;
        }
    }
}
