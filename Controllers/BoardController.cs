﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StakimMVC4.Controllers
{
    public class BoardController : BaseBoardController
    {
        //
        // GET: /Board/

        public ActionResult Index(int? s, int? r)
        {
            return View(FindBoard(s, r, "board"));
        }
    }
}
