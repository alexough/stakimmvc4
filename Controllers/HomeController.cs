﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StakimMVC4.Controllers
{
    public class HomeController : Controller
    {
        private int FATIMA_MESSAGE = 7;
        //
        // GET: /Home/

        /*private DateTime FindLatestDate(StakimDataContext dataContext)
        {
            var query = from board in dataContext.Boards
                        where board.BoardTitle.page == "index"
                        orderby board.week_date descending
                        select board;

            return query.First().week_date;
        }*/

        private DateTime FindLatestDate(StakimDataContext dataContext)
        {
            var query = from board in dataContext.Boards
                        where board.board_title_id == FATIMA_MESSAGE && board.BoardTitle.page == "index"
                        orderby board.week_date descending
                        select board;

            return query.First().week_date;
        }

        private List<Models.SubBoardData> FindSubBoards(StakimDataContext dataContext, Models.BoardData board)
        {
            List<Models.SubBoardData> list = dataContext.SubBoards.Where(s => s.board_id == board.Id).OrderBy(s => s.id)
                .Select(s => new Models.SubBoardData
                {
                    Id = s.id,
                    Board = board,
                    SubItem = s.sub_item
                }).ToList();

            return list;
        }

        public ActionResult Index()
        {
            StakimDataContext dataContext = new StakimDataContext();
            DateTime latestDateTime = FindLatestDate(dataContext);
            DateTime dateTime = DateTime.Today.AddMonths(-1);
            List<Models.BoardData> list = dataContext.Boards
                .Where(b => (b.board_title_id == FATIMA_MESSAGE && b.week_date == latestDateTime) || (b.board_title_id != FATIMA_MESSAGE && b.week_date >= dateTime && b.BoardTitle.page == "index"))
                .OrderBy(b => b.board_title_id)
                .Select(b => new Models.BoardData
                {
                    Id = b.id,
                    BoardTitle = b.BoardTitle.title,
                    Item = b.item,
                    WeekDate = System.DateTime.Parse(b.week_date.ToString()).ToString("yyyy-MM-dd"),
                }).ToList();

            foreach (Models.BoardData board in list)
            {
                List<Models.SubBoardData> sub_board_list = FindSubBoards(dataContext, board);
                board.SubBoards = sub_board_list;
            }

            return View(list);
        }
    }
}
