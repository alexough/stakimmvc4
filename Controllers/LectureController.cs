﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StakimMVC4.Controllers
{
    public class LectureController : Controller
    {
        //
        // GET: /Board/

        public ActionResult Index(int from, int to)
        {
            ViewBag.from = from;
            ViewBag.to = to;

            StakimDataContext dataContext = new StakimDataContext();

            List<SpecialLecture> lectureList = new List<SpecialLecture>();
            DateTime toDate = new DateTime(to + 1, 1, 1);
            if (from == 0)
            {
                lectureList = dataContext.SpecialLectures.Where(l => l.date == null || l.date < toDate)
                    .OrderBy(l => l.date).ThenBy(l => l.id).ToList();
            }
            else
            {
                DateTime fromDate = new DateTime(from, 1, 1);
                lectureList = dataContext.SpecialLectures.Where(l => l.date >= fromDate && l.date < toDate)
                    .OrderBy(l => l.date).ThenBy(l => l.id).ToList();
            }

            return View(lectureList);
        }
    }
}
