﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StakimMVC4.Controllers
{
    public class SchoolController : BaseBoardController
    {
        //
        // GET: /School/

        public ActionResult Index(int? s, int? r)
        {
            return View(FindBoard(s, r, "school"));
        }
    }
}
