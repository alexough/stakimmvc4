﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StakimMVC4.Models
{
    public class BibleGospelData
    {
        public string WeekTitle { get; set; }
        public string WeekDate { get; set; }
        public string WeekDateRaw { get; set; }
        public string GospelTitle { get; set; }
        public List<Models.BibleContentData> ContentList { get; set; }
    }
}