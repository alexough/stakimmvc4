﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StakimMVC4.Models
{
    public class SimpleBibleGospelData
    {
        public string WeekTitle { get; set; }
        public DateTime WeekDate { get; set; }
    }
}