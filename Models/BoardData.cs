﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StakimMVC4.Models
{
    public class BoardData
    {
        public int Id { get; set; }
        public string BoardTitle { get; set; }
        public string Item { get; set; }
        public string WeekDate { get; set; }
        public List<SubBoardData> SubBoards { get; set; }
    }
}