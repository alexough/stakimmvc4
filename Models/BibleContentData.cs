﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StakimMVC4.Models
{
    public class BibleContentData
    {
        public int Chapter { get; set; }
        public int Number { get; set; }
        public string Content { get; set; }
    }
}