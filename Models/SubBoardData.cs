﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StakimMVC4.Models
{
    public class SubBoardData
    {
        public int Id { get; set; }
        public BoardData Board { get; set; }
        public string SubItem { get; set; }
    }
}