﻿
var page_array = new Array("Home", "Mass", "Board", "School", "Resource", "Replay", "Schedule", "Group", "Contact");

$(document).ready(function()
{
    var today = new Date();
    $("#today_text_span").text(today.getFullYear() + "." + (today.getMonth() + 1) + "." + today.getDate());

    $("img[id^='nav_']").click(function()
    {
        var id = $(this).attr("id");
        var index = id.substring(id.length - 1);
        //window.open(page_array[index - 1] + ".aspx", "_self");
        window.open(page_array[index - 1], "_self");
        set_nav_images(index);
    });
});

function set_nav_images(selected_index)
{
    $("img[id^='nav_']").each(function()
    {
        var id = $(this).attr("id");
        var index = id.substring(id.length - 1) * 1;

        if (selected_index == index)
        {
            //window.open(page_array[index - 1] + ".aspx", "_self");
            $(this).attr("src", "/Images/nav_imgs/" + index + "2.jpg");
        }
        else
        {
            $(this).attr("src", "/Images/nav_imgs/" + index + ".jpg");
        }
    });
}