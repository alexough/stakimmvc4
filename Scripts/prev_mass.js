﻿
//var xsl_transformer = new XSLTransformer();

var sermon_file = null;

$(document).ready(function()
{
    //find_list();
});

/*function find_list()
{
    $.ajax({ type: "POST",
        url: "BibleService.asmx/FindMassList",
        dataType: "xml",
        //data: "weekDate=" + week_date,
        async: false,
        processData: false,
        beforeSend: function() { ajax_before_send_find_list(); },
        complete: function(XMLHttpRequest) { ajax_complete_find_list(XMLHttpRequest); },
        error: function(XMLHttpRequest, textStatus, errorThrown) { ajax_error_find_list(XMLHttpRequest, textStatus, errorThrown); },
        success: function(xml, textStatus) { ajax_success_find_list(xml, textStatus); }
    });
}

function ajax_before_send_find_list()
{
    //$("#gospel_div").html('<img src="./res/app/image/loading_small.gif"></img>');
}

function ajax_complete_find_list(XMLHttpRequest)
{
}

function ajax_error_find_list(XMLHttpRequest, textStatus, errorThrown)
{
    $("#week_list_div").html('');
}

function ajax_success_find_list(xml, textStatus)
{
    // display the returned records
    var content_str = xsl_transformer.transform_doc(xml, "resources/xsl/week_list.xslt");
    $("#week_list_div").html(content_str);

    $("#week_list").change(function()
    {
        find_gospel($(this).attr("value"));
    });
}*/

function find_gospel(week_date)
{
    if (week_date == "") return;
    
    $.ajax({ type: "POST",
        url: "/Mass/Gospel/",
        dataType: "html",
        data: "week_date=" + week_date,
        async: false,
        processData: false,
        beforeSend: function() { ajax_before_send_find_gospel(); },
        complete: function(XMLHttpRequest) { ajax_complete_find_gospel(XMLHttpRequest); },
        error: function(XMLHttpRequest, textStatus, errorThrown) { ajax_error_find_gospel(XMLHttpRequest, textStatus, errorThrown); },
        success: function(xml, textStatus) { ajax_success_find_gospel(xml, textStatus); }
    });
}

function ajax_before_send_find_gospel()
{
    //$("#gospel_div").html('<img src="./res/app/image/loading_small.gif"></img>');
}

function ajax_complete_find_gospel(XMLHttpRequest)
{
}

function ajax_error_find_gospel(XMLHttpRequest, textStatus, errorThrown)
{
    $("#gospel_div").html('');
}

function ajax_success_find_gospel(result, textStatus)
{
    // display the returned records
    //var content_str = xsl_transformer.transform_doc(xml, "resources/xsl/mass.xslt");
    $("#gospel_div").html(result);

    /*var splitted = xml.documetElement.getAttribute("week_date").split("-");
    week_date = new Date();
    week_date.setFullYear(splitted[0], (splitted[1] * 1) - 1, splitted[2]);*/
    //sermon_file = xml.documentElement.getAttribute("sermon_file");
    //play("강론 - " + sermon_file, "http://www.stakim.org/resources/sermons/" + sermon_file + "-sermon.wma");
    var extension = 'mp3';
    var week_date = $("#week_date").val();
    var year = parseInt("20" + week_date.substr(6));
    var month = parseInt(week_date.substr(0, 2));
    var day = parseInt(week_date.substr(3, 2));
    if (new Date(year, month, day).valueOf() < new Date(2013, 1, 13).valueOf())
    {
        extension = 'wma';
    }
    play("강론 - " + $("#week_date").val(), "http://www.stakim.org/resources/sermons/" + $("#week_date").val() + "-sermon." + extension);

}