﻿
var list_div = null;
//var xsl_transformer = new XSLTransformer();

var selected_array = new Array();

$(document).ready(function()
{
    list_div = $("#lecture_list_div");

    //find_lectures(0, 1996);
    //find_lectures(2004, 2010);

    /*$("font[id='font_rent']").click(function()
    {
        confirm_rent();
    });*/

});

function find_lectures(from_year, to_year)
{
    $.ajax({ type: "POST",
        url: "/Lecture/",
        data: "from=" + from_year
            + "&to=" + to_year,
        dataType: "xml",
        processData: false,
        beforeSend: function() { ajax_before_send_find_lectures(); },
        complete: function(XMLHttpRequest) { ajax_complete_find_lectures(XMLHttpRequest); },
        error: function(XMLHttpRequest, textStatus, errorThrown) { ajax_error_find_lectures(XMLHttpRequest, textStatus, errorThrown); },
        success: function(xml, textStatus) { ajax_success_find_lectures(xml, textStatus); }
    });
}

function ajax_before_send_find_lectures()
{
    list_div.html('<img src="resources/images/loading_small.gif"></img>');
}

function ajax_complete_find_lectures(XMLHttpRequest)
{
}

function ajax_error_find_lectures(XMLHttpRequest, textStatus, errorThrown)   
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
    list_div.html('<font class="errorMessage">Failed to Get Data!!</font>');
}

function ajax_success_find_lectures(xml, textStatus)
{
    // display the returned records
    var content_str = xsl_transformer.transform_doc(xml, "resources/xsl/lecture_list.xslt");
    list_div.html(content_str);

    check_input();

    $("td[id^='td_year_']").click(function()
    {
        change_selected_tab($(this));
    });

    $(":input").click(function()
    {
        input_checked($(this));
    });
}

/*function check_input()
{
    for (var i = 0; i < selected_array.length; i++)
    {
        $(":input[id='" + selected_array[i][0] + "']").attr("checked", true);
        $(":input[id='" + selected_array[i][0] + "']").parent().parent().addClass("clsPageTextFocus");
    }
}*/

function change_selected_tab(selected_tab)
{
    if (selected_tab.attr("class") == "selected") return;

    var selected_obj = $("td[class='selected']");
    selected_obj.removeClass("selected");
    selected_obj.addClass("unselected");

    selected_tab.toggleClass("selected");
    selected_tab.toggleClass("unselected");

    var fromYear = selected_tab.attr("from");
    var toYear = selected_tab.attr("to");
    find_lectures(fromYear, toYear);
}

/*function input_checked(selected_input)
{
    if(selected_input.attr("checked") && selected_array.length >= 3)
    {
        alert("최대한 3개 파일까지만 신청이 가능합니다.");
        selected_input.attr("checked", false);
        return;
    }

    if(selected_input.attr("checked"))
    {
        selected_array.push(new Array(selected_input.attr("id"), selected_input.attr("title"), selected_input.attr("file_name")));
        selected_input.parent().parent().addClass("clsPageTextFocus");
    }
    else
    {
        remove_selected(selected_input);
        selected_input.parent().parent().removeClass("clsPageTextFocus");
    }
}

function remove_selected(selected_input)
{
    for(var i = 0; i < selected_array.length; i++)
    {
        if(selected_array[i][0] == selected_input.attr("id"))
        {
            selected_array.splice(i, 1);
            return;
        }
    }
}

function confirm_rent()
{
    if (selected_array.length == 0)
    {
        alert("선택하신 파일이 없습니다.");
        return;
    }

    var str = "선택하신 파일은 다음과 같습니다.<br/>";
    str += "<br/>";
    for (var i = 0; i < selected_array.length; i++)
    {
        str += "<font color='blue'>" + selected_array[i][1] + "</font><br/>";
    }
    str += "<br/>";
    str += "선택하신 파일이 일치하면 아래 성명과 연락처를 기입하신후 '신청' 버튼을 눌러 주십시오.<br/>";
    str += "(<font color='red'>*</font> 는 반드시 기입하셔야 합니다.)<br/>";
    str += "<br/>";

    str += "<table class='namelist'>";
    str += "<tr>";
    str += "<td valign='top'>성명<font color='red'>*</font> : </td>";
    str += "<td valign='top'><input type='text' id='input_name'/></td>";
    str += "</tr>";
    str += "<tr>";
    str += "<td valign='top'>전화번호<font color='red'>*</font> : </td>";
    str += "<td valign='top'><input type='text' id='input_phone'/></td>";
    str += "</tr>";
    str += "<tr>";
    str += "<td valign='top'>이메일 : </td>";
    str += "<td valign='top'><input type='text' id='input_email'/></td>";
    str += "</tr>";
    str += "</table>";
    str += "<div class='clsPageSmallRedTextBlk' id='message_div' style='display:none'/>";

    $("#rental_info").html(str);
    $('#rent_div').dialog('open');
}

function do_rent()
{
    $("#message_div").html("");
    $("#message_div").hide();

    var message = "";
    
    if ($("#input_name").attr("value") == "")
    {
        message += "<br/>대출자의 이름이 기입되지 않았습니다.";
    }

    if ($("#input_phone").attr("value") == "")
    {
        message += "<br/>대출자의 연락처가 기입되지 않았습니다.";
    }

    if (message != "")
    {
        $("#message_div").html(message);
        $("#message_div").show();
        return false;
    }

    var selected_ids = "";
    for (var i = 0; i < selected_array.length; i++)
    {
        selected_ids += selected_array[i][0] + ",";
    }

    apply_rent($("#input_name").attr("value"), $("#input_phone").attr("value"), $("#input_email").attr("value"), selected_ids);
    
    return true;
}

function apply_rent(renter_name, renter_phone, renter_email_address, selected_ids)
{
    $.ajax({ type: "POST",
        url: "LectureService.asmx/Apply",
        data: "renterName=" + renter_name
            + "&renterPhone=" + renter_phone
            + "&renterEMailAddress=" + renter_email_address
            + "&lectureIds=" + selected_ids,
        dataType: "xml",
        async: false,
        processData: false,
        beforeSend: function() { ajax_before_send_apply_rent(); },
        complete: function(XMLHttpRequest) { ajax_complete_apply_rent(XMLHttpRequest); },
        error: function(XMLHttpRequest, textStatus, errorThrown) { ajax_error_apply_rent(XMLHttpRequest, textStatus, errorThrown); },
        success: function(xml, textStatus) { ajax_success_apply_rent(xml, textStatus); }
    });
}

function ajax_before_send_apply_rent()
{
}

function ajax_complete_apply_rent(XMLHttpRequest)
{
}

function ajax_error_apply_rent(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
    $("#confirm_div").html('<font class="errorMessage">자료 신청이 실패하였습니다. 다시 시도해 주시기 바랍니다.</font>');
}

function ajax_success_apply_rent(xml, textStatus)
{
    $("#confirm_info").html(xml.documentElement.firstChild.nodeValue);
}*/

/*function play_lecture(file_name, short_file_name, auto_start)
{
    //short_file_name = 'http://stakim.org/resources/Lectures/' + short_file_name;
    //short_file_name = 'http://dl.dropbox.com/u/13567410/' + short_file_name;
    short_file_name = 'http://' + short_file_name;

    var browser = navigator.appName;

    var html = "";
    if (browser.indexOf("Microsoft") != -1)
    {
        html = "<OBJECT id='MediaPlayer' width='310' height='42' CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' type='application/x-oleobject'>";
        html += "<PARAM NAME='URL' VALUE='" + short_file_name + "'>";
        html += "<PARAM NAME='SendPlayStateChangeEvents' VALUE='True'>";
        html += "<param name='Showcontrols' value='True'>";
        html += "<param name='WindowlessVideo' value='False'>";
        //html += "<PARAM NAME='AutoStart' VALUE='True'>";
        if (auto_start)
        {
            html += "<param name='AutoStart' value='True'>";
        }
        else
        {
            html += "<param name='AutoStart' value='False'>";
        }
        html += "<PARAM name='uiMode' value='full'>";
        html += "</OBJECT>";
    }
    else
    {
        html = "<object  data='" + short_file_name + "' type='video/x-ms-wmv' width='320' height='65'>";
        html += "<param name='ShowStatusBar' value='1'>";
        html += "<param name='src' value='" + short_file_name + "'>";
        if (auto_start)
        {
            html += "<param name='autostart' value='1'>";
        }
        else
        {
            html += "<param name='autostart' value='0'>";
        }
        html += "<param name='volume' value='0'>";
        html += "</object>";
    }

    //$("#play_title_div").html(file_title);
    html = '잠시 기다리시면 강론이 자동으로 시작됩니다.<br/><br/>' + html + '<br/><br/><center>' + file_name + '</center>';
    $("#play_div").html(html);
    $('#play_div').dialog('open');
}*/