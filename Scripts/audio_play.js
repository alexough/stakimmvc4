﻿
function play_prayer(file_to_play, title)
{
    if (file_to_play == "") return;

    play(title, file_to_play, true);
}

function play(file_title, file_name, auto_start)
{
    var html = "";
    var browser = navigator.appName;
    var is_mp3 = file_name.indexOf('.mp3') > 0;

    if (!is_mp3)
    {
        if (browser.indexOf("Microsoft") != -1)
        {
            html = "<OBJECT id='MediaPlayer' width='320' height='65'";
            html += " classid='CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95'";
            html += " codebase='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=,1,52,701'";
            html += " standby='Loading Microsoft Windows Media Player components...' type='application/x-oleobject'>";
            html += "  <PARAM NAME='FileName' value='" + file_name + "'>";
            html += "  <PARAM NAME='ShowControls' VALUE='1'>";
            html += "  <PARAM NAME='ShowDisplay' VALUE='0'>";
            html += "  <PARAM NAME='ShowStatusBar' VALUE='1'>";
            html += "  <PARAM NAME='ShowGoToBar' VALUE='0'>";
            html += "  <param name='uiMode' value='mini'>";
            html += "  <PARAM NAME='TransparentatStart' VALUE='True'>";
            html += "  <PARAM NAME='SendPlayStateChangeEvents' VALUE='True'>";
            if (auto_start)
            {
                html += "<param name='AutoStart' value='True'>";
            }
            else
            {
                html += "<param name='AutoStart' value='False'>";
            }
            html += "  <PARAM NAME='InvokeURLs' VALUE='false'>";
            html += "</OBJECT>";
        }
        else
        {
            html = "<object id='MediaPlayer' data='" + file_name + "' type='video/x-ms-wmv' width='320' height='65'>";
            html += "<param name='ShowStatusBar' value='1'>";
            html += "<param name='src' value='" + file_name + "'>";
            if (auto_start)
            {
                html += "<param name='autostart' value='1'>";
            }
            else
            {
                html += "<param name='autostart' value='0'>";
            }
            html += "<param name='volume' value='0'>";
            html += "</object>";
        }
    }
    else
    {
        html = '<object type="application/x-shockwave-flash" data="http://www.stakim.org/resources/sermons/1pixelout.swf" width="400" height="50" >';
        html += '<param name="movie" value="http://www.stakim.org/resources/sermons/1pixelout.swf" />';
        html += '<param name="wmode" value="transparent" />';
        html += '<param name="menu" value="false" />';
        html += '<param name="quality" value="high" />';
        html += '<param name="FlashVars" value="soundFile=' + file_name + '" />';
        html += '<embed src="http://www.stakim.org/resources/sermons/1pixelout.swf" flashvars="soundFile=' + file_name + '" width="400" height="50" />';
        html += '</object>';
    }

    if (auto_start)
    {
        $("#play_title_div").html(file_title);
    }
    $("#play_list_div").html(html);
}